#!/usr/bin/python

import pandas as pd

inputcsv = "sberro_small_genes.csv"
csv = pd.read_csv(inputcsv)

fasta = [">%s\n%s\n" % (x[0], x[1]) for x in zip(csv['Small_protein_family_ID'], csv["Sequence"])]

open("sberro_small_genes.faa", 'w').write("".join(fasta))
