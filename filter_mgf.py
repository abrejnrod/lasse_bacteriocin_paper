#!/usr/bin/python3

from pyteomics.mgf import IndexedMGF, write
mgf_file = "/Users/chs962/Downloads/ProteoSAFe-METABOLOMICS-SNETS-V2-c999bce5-download_clustered_spectra/METABOLOMICS-SNETS-V2-c999bce5-download_clustered_spectra-main.mgf"
mgf = IndexedMGF(mgf_file)

new_mgf = []
for index, entry in enumerate(mgf):
    if entry['m/z array'].size == 0:
        continue
    new_mgf.append(entry)


write(new_mgf, output = "human_microbiome_isolates.mgf")

print("done")
