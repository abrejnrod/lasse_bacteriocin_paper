# -*- coding: utf-8 -*-



import sys
import pandas as pd
from Bio import Entrez
import argparse
import time
import csv
from pandas import read_excel
from Bio import SeqIO
import ftputil
import json
import time

def get_assembly_id(acc_id):
    """Get Assembly ID from Accession ID"""
    handle = Entrez.esearch(db='biosample', term=acc_id)
    record = Entrez.read(handle)
    handle.close()
    biosample_uid = record["IdList"][0]
    handle = Entrez.elink(dbfrom='biosample', id=biosample_uid, linkname = "biosample_assembly")
    record = Entrez.read(handle)
    handle.close()
    assembly = record[0]["LinkSetDb"][0]["Link"][0]["Id"]
    return assembly

def get_assembly_summary(acc_id):
    """Get esummary from an assembly ID"""
    from Bio import Entrez
    #provide your own mail here
    Entrez.email = "carmen.ausejo@sund.ku.dk" #email
    assembly = get_assembly_id(acc_id)
    esummary_handle = Entrez.esummary(db="assembly", id=assembly, report="full")
    esummary_record = Entrez.read(esummary_handle)
    genbank_id = esummary_record['DocumentSummarySet']['DocumentSummary'][0]['Synonym']['Genbank']
    refseq_id = esummary_record['DocumentSummarySet']['DocumentSummary'][0]['Synonym']['RefSeq']
    ftp_path = esummary_record['DocumentSummarySet']['DocumentSummary'][0]["FtpPath_RefSeq"]
    return([ assembly, genbank_id, refseq_id,ftp_path])

host = ftputil.FTPHost('ftp.ncbi.nlm.nih.gov', 'anonymous', 'password')

iomega = json.load(open("human_microbiome_isolates_iomega.json", "r"))

for index, genome in enumerate(iomega["genomes"]):
#    if index > 4:
#        break
    biosample = genome["BioSample_accession"]
    pretty_name = genome["genome_label"].replace(" ", "-").replace("/", "")
    es = get_assembly_summary(biosample)
    asm_name = es[3].split("/")[-1]
    ftp_path = "/".join(es[3].split("/")[3:]) + "/" + asm_name + "_protein.faa.gz"
#    print(biosample, pretty_name, es)
    print(ftp_path)
    time.sleep(30)

    host.download(ftp_path, "human_microbiome_proteins/" + pretty_name + "_" +asm_name+ "_protein.faa.gz")
host.close()